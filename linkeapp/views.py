from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from social_core.utils import get_strategy


def login(request):
    return render(request, 'login.html', {'request': request})

@login_required(login_url='login')
def home(request):
    return render(request, 'home.html')

def logout(request):
    auth_logout(request)
    return redirect('/')

def auth_callback(request, backend):
    strategy = get_strategy(request)
    try:
        user = strategy.complete(backend)
        if user and user.is_active:
            return HttpResponse("Authentication successful!")
        else:
            return HttpResponse("Authentication failed!")
    except Exception as e:
        return HttpResponse("An error occurred during authentication.")
