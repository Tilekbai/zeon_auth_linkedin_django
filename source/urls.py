from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from linkeapp.views import login, logout, home, auth_callback

urlpatterns = [
    path('admin/', admin.site.urls),
    path('social-django/', include('social_django.urls', namespace='social')),
    path('', login, name='login'),
    path('home/', home, name='home'),
    path('logout/', logout, name='logout'),
    path('complete/linkedin-oauth2/', auth_callback, name='auth_callback'),

]

